const express = require("express");
const app = express();
const mongoose = require("mongoose");
require("dotenv").config();
require("./config/database");

app.use(express.json());

const Cat = mongoose.model("Cat", { name: String });

// const kitty = new Cat({ name: "Zildjian" });
// kitty.save().then(() => console.log("meow"));

app.use("/", async (req, res) => {
  const findCat = await Cat.find();
  res.json({ cat: findCat });
});

app.listen(process.env.PORT || 3000, () => {
  console.log("server has started at port " + `${process.env.PORT || 3000}`);
});
